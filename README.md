![Made with Node.JS](https://img.shields.io/badge/made%20with-Node.JS-brightgreen)
# Reflect

  

***An Discord server cloner made with Node.JS and Discord.js V11***

  

# Requirements -

- Node.JS (preferably the latest version)

- A PC / Laptop or a phone ([The replit Version is out](https://replit.com/@Sakuu/Reflect))

- Your discord account and its Token ([Click here if you do not know how to get one.](https://www.youtube.com/watch?v=YEgFvgg7ZPI))

  

# Installation

  

1. Download the repo and extract it wherever you want

 2. Open up the folder and run the batch files for the dependencies if on windows. else use npm install

3. After it has downloaded all the dependencies click on start.bat (Or open the folder in the command prompt and do node Main.js)

4. Enter the necessary info for it to clone (Your account's token , The server to be cloned's ID and the target server's ID)

5. Now just press enter and get a exact clone of the server!

  

# Replit version (or how do use it on a phone)

Much easier to use -

  

1. Go to the repl

2. Fork it

3. Run it and let it install the dependencies

4. Enter details 

5. press enter and let it do its job

6. Now you have your clone!

  
  

# Features -

  

- Clones every channel Incl. VCs (With the topics)

- Clones emojis

- Does not steal your token or save it locally 

- no this cannot clone stickers this is made with v11 what do u expect?

  



  

###### pls someone make this thing better idk what to do now